# Helm Rollback Success and Failure Demo

This project demos a mock pipeline where a "helm rollback" is executed when 
upstream acceptance job fails.

You are invited to run the pipeline over and over to test possible results.

## Job Pass or Fail

The `deploy-job` and `acceptance-job` use a "flip of the coin to pass and fail.

A job script generates a random number between 1-100. If the number is greater-than
or equal-to `50`, the job passes. Otherwise it fails.

```shell
  function pass_fail() {
    if [[ -n $random_number && $random_number -ge 50 ]]
    then
      log_info "Pass, $random_number >= 50"
      exit 0
    else
      log_error "Fail, $random_number < 50"
      exit 1
    fi
  }
```

## Job Behavior

If the `deploy-job` passes, the `acceptance-job` runs.

![img](imgs/deploy-passes.png)

[Sample Pipeline](https://gitlab.com/timothy.stone/helm-rollback-demo/-/pipelines/980258358)

***

If the `acceptance-job` fails, representing a failed test in the deployment
environment, the `rollback-job` **runs**. 

![img](imgs/rollback-runs.png)

[Sample Pipeline](https://gitlab.com/timothy.stone/helm-rollback-demo/-/pipelines/980220790)

***

If the `acceptance-job` passes, the `rollback-job` is **skipped**.

![img](imgs/acceptance-passes.png)

[Sample Pipeline](https://gitlab.com/timothy.stone/helm-rollback-demo/-/pipelines/980220313)

***

If the `deploy-job` fails, both the `acceptance-job` and `rollback-job` are **skipped**.

![img](imgs/deploy-fails.png)

[Sample Pipeline](https://gitlab.com/timothy.stone/helm-rollback-demo/-/pipelines/980195536)

***

The `rollback-job` leverages `needs`, GitLab's Directed Acyclic Graph (DAG) keyword. The 
job "needs" the `acceptance-job` to "fail" (`when: on_failure`).

## Use of `needs`

`needs` should be _very_ carefully used, thus `needs` as not often seen in pipelines, more often
it may be seen as `needs: []`. This signals GitLab that job does not rely on other jobs and
can be started _immediately_. Always remember, using `needs` in any form will mean that 
[artifacts from upstream jobs will be unavailable to the job](https://docs.gitlab.com/ee/ci/yaml/index.html#needsartifacts).

## Test Yourself

Run the pipeline, over and over, or retry a failed one, to watch the behaviors.